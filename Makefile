# This is a makefile for attiny2313 using bus pirate as a programmer

CC=/usr/bin/avr-gcc
OBJCOPY=/usr/bin/avr-objcopy
SIZE=/usr/bin/avr-size
AVRDUDE=/usr/bin/avrdude
GDB=/usr/bin/avr-gdb
SIMAVR=/usr/bin/simavr
#PGM=avrisp
PGM=buspirate
MCU_VERS=85
CFLAGS=-Wall -pedantic -O0 -mmcu=attiny$(MCU_VERS) -Iinclude
LDFLAGS=
SIMAVR_OPTS=-v -g -m attiny$(MCU_VERS) -f 8000000
GDB_OPTS=-nh -iex "set auto-load safe-path ."

# USB device to connect to Arduino
#DEV=/dev/ttyACM0
DEV=/dev/ttyUSB0

# Source files
SRC=$(wildcard src/*.c)
OBJ=$(subst .c,.o,$(SRC))
AVROBJ=$(subst .c,.avro,$(SRC))

# Output basename
OUT=output
HEX=$(OUT).hex
AXF=$(OUT).axf

#${CC} -Os -DF_CPU=1000000UL -mmcu=attiny2313 -c -o $(OUT).o $(CFLAGS) $(SRC)

.PHONY: build
build: $(HEX)
	${SIZE} --mcu=attiny$(MCU_VERS) -C --format=avr $(OUT)

$(OUT): $(OBJ)
	${CC} -mmcu=attiny$(MCU_VERS) $(LDFLAGS) $(OBJ) -o $(OUT)

$(HEX): $(OUT)
	${OBJCOPY} -O ihex -R .eeprom $(OUT) $(HEX)

.PHONY: upload
upload: build
	${AVRDUDE} -F -V -c $(PGM) -p ATTINY$(MCU_VERS) -P $(DEV) \
				-U flash:w:$(OUT).hex

%.avro: %.c
	${CC} $(CFLAGS) -gdwarf-2   -ffunction-sections -fdata-sections \
	    -DF_CPU=10000000 -c -o $@ $<

.PHONY: simavr
simavr: $(AVROBJ)
	${CC} -mmcu=attiny$(MCU_VERS) $(LDFLAGS) -gdwarf-2  -o $(AXF)  $(AVROBJ) -Wl,--relax,--gc-sections -Wl,--undefined=_mmcu,--section-start=.mmcu=0x910000

.PHONY: debug
debug: simavr
	(${SIMAVR} $(SIMAVR_OPTS) $(AXF) &)
	sleep 2
	${GDB} $(GDB_OPTS) $(AXF)
	# kill simavr background process associated with this shell
	kill `ps | grep simavr | sed -e 's/^ *//' | cut -f1 -d' '`

.PHONY: read
read:
	${AVRDUDE} -F -V -c $(PGM) -p ATTINY$(MCU_VERS) -P $(DEV) \
	    -U flash:r:read.hex:i
.PHONY: clean
clean:
	rm -rf $(OUT) $(HEX) $(OBJ) $(AVROBJ) $(AXF)

