`simple_sched`: Simple scheduler project for AVR

Warning: This is a toy project, work in progress and not designed for actual 
use in the real world.


Design
----
 - the scheduler maintains a queue of processes (a list of struct * pthread)
   and runs them in a round robin fashion
 - each process or thread has to be registered at startup in the queue by
   instantiating a struct pthread and registering it to the scheduler
 - the execution time of each thread is constant and defined by the constant
   `THREAD_EXEC_TIME` which is tunable
 - a timer counts the execution time in ms and sets an external interrupt when
   necessary, allowing process switching
 - each process has his own stack, for now statically allocated, which is used 
   for storing GPRs and status register during context switching. 
   The `WITH_HEAP` option aims at providing a `malloc`'ed stack at thread 
   creation, not functional yet.


Status
----
The scheduler has been quickly tested in simavr and seems to work properly when
compiled with no optimisation at all (-O0).
The thread properly receives its argument through a `void *` pointer access.
As for now, the code is written for attiny85.


TODO
----
 - Implement heap based stack
 - Implement concurrent access of shared variables
 - Test on real hardware
 - Make the scheduler portable on other AVR MCUs
 - Test with -Os optimisation
