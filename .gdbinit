#connect to simavr
target remote :1234

#setup display
display $SP
display $pc
#GPRs
display /32bx 0x800000
#bottom of the stack
display /-32bx 0x80025f
#top of the stack
display /8bx $sp+1

b main
c

#specific pthread_init debugging
#b pthread_init
#c
#display t_queue
#display *t
#display /x t->ctx

#ISR debugging
#b __vector_10
b __vector_2
display ms_counter
display i
display j
#display /t SREG
#display /t GIMSK
#display /t PCMSK
#display /t GIFR
