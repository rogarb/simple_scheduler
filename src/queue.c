/* queue.c: implementation of queue management functiions */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"
#include "errcodes.h"
#include "pthread.h"
#include "config.h" 

/* main queue for the scheduled threads */
/* at first, all pointers in the queue are NULL as it is empty */
struct thread_queue t_queue = {0};

#if 0
void queue_init(void)
{
    t_queue = (struct thread_queue *) malloc(sizeof(struct thread_queue));
    if (t_queue)
        memset(&t_queue, 0, sizeof(t_queue));
}
#endif

int is_queue_empty(void)
{
    int ret = 0;
    if (t_queue.head == NULL)
	ret = 1;

    return ret;
}

int queue_safety_check(struct pthread *t)
{
    if (t == NULL) 
	return -ERR_NULL;

    return 0;
}

int is_thread_queued(struct pthread *t)
{
#ifdef WITH_HEAP
    struct queue_node *p;
#else 
    struct pthread *p;
#endif
    
    for (p = t_queue.head ; p->next != NULL ; p = p->next) {
#ifdef WITH_HEAP
	if (p->thread == t)
#else
	if (p == t)
#endif
	    return 1;
    }

    return 0;
}

#ifdef WITH_HEAP
/* free a queue_node memory slot: we have to ensure that all the allocated
 * memory pointed by members of this structure is freed before freeing the 
 * upper level */
void free_node(struct queue_node *node)
{
    /* TODO: implement struct pthread free */
    pthread_free(node->thread);
    free(node);
}
#endif /* WITH_HEAP */

/* add a thread to the queue 
 * returns 0 on success, <0 on failure */
int thread_queue(struct pthread *t)
{
#ifdef WITH_HEAP
    struct queue_node *new = malloc(sizeof(struct queue_node));
#endif
    int ret = queue_safety_check(t);

    if (ret)
	return ret;

#ifdef WITH_HEAP
    if (new == NULL)
	return -ERR_NOMEM;

    memset(new, 0, sizeof(struct queue_node));
    new->thread = t;
#endif /* WITH_HEAP */

    if (is_queue_empty()) {
#ifdef WITH_HEAP
	t_queue.head = new;
	t_queue.current = new;
	t_queue.tail = new;
#else
	t_queue.head = t;
	t_queue.current = t;
	t_queue.tail = t;
#endif /* WITH_HEAP */
    } else if (is_thread_queued(t)) {
	return -ERR_ALREADYQUEUED;
    } else {
#ifdef WITH_HEAP
	new->prev = t_queue.tail;
	t_queue.tail->next = new;
	t_queue.tail = new;
#else
	t->prev = t_queue.tail;
	t_queue.tail->next = t;
	t_queue.tail = t;
#endif /* WITH_HEAP */
    }
    return ret;
}

int thread_unqueue(struct pthread *t)
{
#ifdef WITH_HEAP
    struct queue_node *p;
#else
    struct pthread *p;
#endif
    int ret = queue_safety_check(t);

    if (ret)
	return ret;

    for (p = t_queue.head ; p->next == NULL ; p = p->next) {
#ifdef WITH_HEAP
	if (p->thread == t) { 
	    struct queue_node *pnext = p->next;
	    struct queue_node *pprev = p->prev;
#else
	if (p == t) { 
	    struct pthread *pnext = p->next;
	    struct pthread *pprev = p->prev;
#endif /* WITH_HEAP */

	    if (p == t_queue.tail) 
		t_queue.tail = pprev;
	    else
		pnext->prev = pprev;
	    if (p == t_queue.head) 
		t_queue.head = pnext;
	    else
		pprev->next = pnext;

	    if (p == t_queue.current) {
		if (p == t_queue.head)
		    t_queue.current = pnext;
		else
		    t_queue.current = pprev;
	    }
#ifdef WITH_HEAP
	    free_node(p);
#endif
	    break;
	}
    }

    return ret;
}

