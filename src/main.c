#include <stdint.h>
#include <stdio.h>
#include "sched.h"
#include "pthread.h"
#include "queue.h"
#include "config.h"

int test_func1(void *data)
{
    uint32_t j_ini = *((uint32_t *) data);
    uint32_t j = j_ini;

    while (1) {
	j++;
	if (j >= UINT32_MAX - 100)
	    j = j_ini;
    }
}

int test_func2(void *data)
{
    uint8_t i = 0xFF;

    while (1) {
	i--;
	if (i < 15)
	    i = 0xFF;
    }
}

int main(void) 
{
#ifdef WITH_HEAP
    struct pthread *t1 = pthread_create(test_func1, NULL, NULL);
    struct pthread *t2 = pthread_create(test_func2, NULL, NULL);

    thread_queue(t1);
    thread_queue(t2);
#else
    struct pthread t1, t2;
    uint32_t i = 2;
    pthread_init(&t1, test_func1, &i);
    pthread_init(&t2, test_func2, NULL);

    thread_queue(&t1);
    thread_queue(&t2);
#endif

    init_sched();
    start_sched();
    return 0;
}
    
