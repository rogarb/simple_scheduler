/* extint.c - implementation of external interrupt handling */

#include <avr/io.h>
//#include "registers.h"
#include "config.h"

void extint_setup(void)
{
    DDRB &= ~(1 << EXT_INT_PIN);    /* input pin */
    PORTB &= ~(1 << EXT_INT_PIN);   /* Hi-Z state */
    GIMSK |= (1 << PCIE);	    /* enable pin change interrupt */
    PCMSK |= (1 << EXT_INT_PIN);    /* setup pin */
    GIFR = 0;			    /* clear flags */
}

