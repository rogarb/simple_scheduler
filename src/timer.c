/* timer.c: low level implementation of scheduler timing on attiny85*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "timer.h"
#include "registers.h"
#include "extint.h"

volatile uint16_t ms_counter;

void timer_init(void)
{
     /* setup and start timer0 in Clear Timer on Compare (CTC) mode
     * with a period of 125 steps and a prescaler of 1:8, so the timer
     * is cleared every ms */
    TCCR0A = 0x02;  /* CTC mode */
    TCCR0B = 0x02;  /* prescaler 1:8 */
    TCNT0 = 0;
    OCR0A = 125;

    ms_counter = 0;
}

void timer_on(void)
{
    TIFR &= ~(1 << OCIE0A); /* clear interrupt flag */
    TIMSK |= (1 << OCF0A);   /* enable output compare interrupt */
    sei();                   /* start global interrupts */
}

void timer_off(void)
{
    TIMSK &= ~(1 << OCF0A);  /* disable output compare interrupt */
    TIFR &= ~(1 << OCIE0A); /* clear interrupt flag */
}

ISR(TIMER0_COMPA_vect) 
{
    if (ms_counter < THREAD_EXEC_TIME) {
	ms_counter++;
    } else {
	ms_counter = 0;
	extint_trigger();
    }
}
