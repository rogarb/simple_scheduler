/* pthread.c : threads implementation */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "pthread.h"
#include "config.h"
#include "context.h"

#ifdef WITH_HEAP
/* allocates a struct pthread and returns a pointer to it */
struct pthread *pthread_create(int (*func)(void *), void *data, void (*free_d)(void *))

#else
struct pthread *pthread_init(struct pthread *t, int (*func)(void *), void *data)
#endif
{
    if (func == NULL)
	return NULL; /* do not allow creation of thread without a function */

#ifdef WITH_HEAP
    struct pthread *t = malloc(sizeof(struct pthread));
    uint8_t *stack = malloc(sizeof(uint8_t)*STACK_SIZE);
    if (t && stack) {
	memset(stack, 0, sizeof(*stack));
#else
//	uint8_t *stack = t->ctx.stack;
#endif
	memset(t, 0, sizeof(*t));
	t->thread_fnct = func;
	t->thread_data = data;
#ifdef WITH_HEAP
	t->free_data = free_d;
	t->ctx.stack = stack;
#endif
	setup_context(&t->ctx, func, data);
#ifdef WITH_HEAP
    }
#endif
    return t;
}

#ifdef WITH_HEAP
/* frees the allocated struct pthread ensuring that the thread data is freed */
void pthread_free(struct pthread *t)
{
    if (t) {
	if (t->free_data)
	    t->free_data(t->thread_data);
	free(t);
    }
}
#endif /* WITH_HEAP */
