/* sched.c: implementation of the scheduler */

#include <stdio.h>
#include <avr/interrupt.h>
#include "sched.h"
#include "queue.h"
#include "timer.h"
#include "registers.h"
#include "config.h"
#include "context.h"
#include "extint.h"

#define LOAD()	__asm__ __volatile__("reti")

void init_sched(void) 
{
    if (QUEUE_CURRENT_NULL)
	return;		/* TODO: implement panic ??? */

    extint_setup();    
    timer_init();
    //timer_on(); /* this goes to start_shed() */
}

/* this routine loads the first thread from a properly setup context
 * the subsequent threads will be loaded by the ISR 
 */
#define LOAD_HEAD_THREAD() \
    do {\
	struct pthread *t = t_queue.head;\
	if (t == NULL)\
	    return;\
	SAVE_GLOBAL_SP();\
	RESTORE_CONTEXT(t->ctx);\
	__asm__ __volatile__("ret");\
    } while (0)

void start_sched(void)
{
    /* we just start the timer and load the first thread */
    timer_on();
    LOAD_HEAD_THREAD();
}

/* This is the core of the scheduler */
ISR(PCINT0_vect, ISR_NAKED) 
{
/* unprotect the access to the queue as the ISR is naked and it would 
 * modify the status register. We assume that if we get here, the current 
 * thread pointer is not NULL */
//    if (QUEUE_CURRENT_NODE != NULL) {
	SAVE_CONTEXT(CURRENT_CTX);
	RESTORE_GLOBAL_SP();
	QUEUE_NEXT();
	SAVE_GLOBAL_SP();
	RESTORE_CONTEXT(CURRENT_CTX);
	LOAD();
//  }
}

#if 0
void run_next(void)
{
    struct pthread *t = get_next_thread();

    if (!t)
	return;
    
    if (t->flags & THREAD_STARTED == 0) {
	t->thread_fnct(t->thread_data);
    } else {
	restore_context(t->ctx);
    }
    return;
}
#endif

