/* context setting and manipulation related functions */

#include <stdio.h>
#include <stdint.h>
#include "context.h"
#include "config.h"
#include "registers.h"

volatile uint16_t global_sp;

void setup_context(struct thread_context *ctx, int (*f)(void *), void *data)
{
    if (ctx == NULL)
	return;

    uint8_t *sp;
#ifndef WITH_HEAP
    /* define the thread's stack pointer */
    //sp = ctx->stack + STACK_SIZE - 1;

    /* we use the address of the sp struct member to easily get the beginning 
     * of the empty stack as sp is placed right after stack[] in the struct 
     * definition */
    sp = (uint8_t *) &ctx->sp;
    sp--;   /* decrease the sp to point the first empty location so it is 
	     * ready to use */
#else
    /* TODO: properly define sp value when heap allocated stack is used */
#endif

    /* we need to initialize some of the context as the thread is started
     * through context switching (either from ret assembly instruction when 
     * first thread is started or by reti from the ISR) and not by regular 
     * function call 
     * */

    uint16_t function_address = (uint16_t) f; 
    uint8_t status_reg = 0x80; /* empty status reg with interrupt enable */
    uint16_t data_address = (uint16_t) data;
    int offset = 0;	/* used for caculating the GPR position on the stack */

    /* load the address of the function on the stack so the ISR can 
     * return to it */
    *sp-- = LOW8BITS(function_address);  /* /!\ decrease the sp as we fill the stack */
    *sp-- = HIGH8BITS(function_address);

    /* load the argument of the function into r25:r24 */
    offset = 24;
    *(sp-offset) = LOW8BITS(data_address);
    offset = 25;
    *(sp-offset) = HIGH8BITS(data_address);

    /* move sp after saved registers */
    sp -= REG_NUM;  /* 32 in attiny85 */

    /* setup status register */
    *sp-- = status_reg;

    /* store stack pointer value in the struct */
    ctx->sp = (uint16_t) sp; 
}
