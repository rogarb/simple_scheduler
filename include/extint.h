/******************************************************************************
* extint.h: Low level external interrupt handling
 *****************************************************************************/
#ifndef EXTINT_H
#define EXTINT_H

#include <avr/io.h>
//#include "registers.h"
#include "config.h"

void extint_setup(void);

#define extint_trigger()    \
    do { \
	/*int eipin = EXT_INT_PIN;*/ \
	/*__asm__ __volatile__ ("sbi 0x16, %0" : : "r" (eipin));*/\
	PINB = (1 << EXT_INT_PIN);\
    } while(0)

#endif /* End of EXTINT_H */
