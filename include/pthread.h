/******************************************************************************
 * pthread.h : Definition of the structure and API to process threads
 ******************************************************************************/
#ifndef PTHREAD_H
#define PTHREAD_H

#include "config.h"
#include "context.h"
#include <stdint.h>

#if 0
/* control flags for thread status */
enum sflags {
    THREAD_STARTED = (1 << 0),
    THREAD_FINISHED = (1 << 1),
};
#endif

/* struct pthread: structure of a thread run by the scheduler */
struct pthread {
    struct pthread *next, *prev;/* allows chaining in the queue */
    int (*thread_fnct)(void *); /* thread function */
    void *thread_data;		/* data to be passed to the thread function */
#ifdef WITH_HEAP
    void (*free_data)(void *);	/* function for freeing thread_data if 
				 * dynamically allocated, NULL if not */
#endif
    struct thread_context ctx;  /* structure for storing the exec context */
//    uint8_t flags;	/* control flags defined in enum sflags */
};

#ifdef WITH_HEAP
struct pthread *pthread_create(int (*func)(void *), void *data, void (*free)(void *));
void pthread_free(struct pthread *t);
#else
struct pthread *pthread_init(struct pthread *t, int (*func)(void *), void *data);
#endif /* WITH_HEAP */

#endif /* End of PTHREAD_H */
