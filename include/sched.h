/******************************************************************************
* sched.h: Scheduler API
******************************************************************************/
#ifndef SCHED_H
#define SCHED_H

void init_sched(void);
void start_sched(void);

#define stop_sched()	timer_off()	/* probably not a useful function */

#if 0
#define LOAD_THREAD()	    do {					      \
				*((uint16_t *) 0x800030) = CURRENT_CTX.pc;			      \
				__asm__ volatile("ijmp");		      \
			    } while(0)
#endif

#endif /* End of SCHED_H */
