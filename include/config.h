/******************************************************************************
* config.h: Configurable definitions for simple scheduler
 *****************************************************************************/
#ifndef CONFIG_H
#define CONFIG_H
#include <avr/io.h>

#ifdef WITH_HEAP
    #undef WITH_HEAP
#endif

#define THREAD_EXEC_TIME 10	/* thread execution time in ms */
#define EXT_INT_PIN	0	/* use pin 0 on port B as the interrupt pin */

#endif /* End of CONFIG_H */
