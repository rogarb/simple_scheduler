/******************************************************************************
* context.h: Low level context definition and context manipulation macros
 *****************************************************************************/
#ifndef CONTEXT_H
#define CONTEXT_H

#include <stdint.h>
#include <avr/io.h>
#include "registers.h"
#include "config.h"

#ifdef WITH_HEAP
    #include "queue.h"
#endif

#define STACK_SIZE 128	/* size of stack in bytes, TODO: find a way to compute 
			 * a per thread value for heap allocation */
/* in attiny85 there is only 512 bytes of SRAM, 128 bytes per thread might look
 * huge, but we use the stack to store everything, like registers content, sp
 * address, status register and program counter address: only this requires at
 * least 32 + 2 + 1 + 2 = 37 bytes */

/* struct thread_context: encapsulates all the context information needed to 
 * start, stop and restart a thread 
 * We maintain a per thread stack, which contains all the execution context and
 * the proper return address from ISR or the launching routine */
struct thread_context {
//    uint16_t pc; /* program counter */ // we don't need to save the PC 
//    uint8_t gpr[REG_NUM];	/* CPU general purpose registers */
//    uint8_t status_reg;	/* status register */ 
#ifndef WITH_HEAP
    uint8_t stack[STACK_SIZE]; /* fixed size stack */
#else
    uint8_t *stack;	/* allocated stack for the thread, so it is on the heap
    			 * and doesn't mess with the MCU stack */
#endif /* WITH_HEAP */
    uint16_t sp;	/* stack pointer content: has to be after the fixed 
			 * stack in the struct as we use its address to 
			 * calculate the end of the stack */
};

extern volatile uint16_t global_sp;

void setup_context(struct thread_context *ctx, int (*f)(void *), void *data);

/* these macros manipulate the context of a thread in the attiny85: we use 
 * macros and not functions as we manipulate the program counter through the 
 * stack, and the stack is modified with function calls
 * These macros are to be called by the ISR which is the core of the 
 * scheduler */
/* this is moved to ISR assembly definition */
#define SAVE_CONTEXT(ctx) \
    do {\
    __asm__ __volatile(\
"   push    r0\n"	/* push all the registers on the stack */\
"   push    r1\n"\
"   push    r2\n"\
"   push    r3\n"\
"   push    r4\n"\
"   push    r5\n"\
"   push    r6\n"\
"   push    r7\n"\
"   push    r8\n"\
"   push    r9\n"\
"   push    r10\n"\
"   push    r11\n"\
"   push    r12\n"\
"   push    r13\n"\
"   push    r14\n"\
"   push    r15\n"\
"   push    r16\n"\
"   push    r17\n"\
"   push    r18\n"\
"   push    r19\n"\
"   push    r20\n"\
"   push    r21\n"\
"   push    r22\n"\
"   push    r23\n"\
"   push    r24\n"\
"   push    r25\n"\
"   push    r26\n"\
"   push    r27\n"\
"   push    r28\n"\
"   push    r29\n"\
"   push    r30\n"\
"   push    r31\n"\
"   in	    r0,	0x3f\n"	/* status register */\
"   push    r0\n"	/* push status register content */\
);\
	ctx.sp = SP;\
    } while (0)

#define SAVE_GLOBAL_SP()    do { global_sp = SP; } while (0)

#define RESTORE_GLOBAL_SP() do { SP = global_sp; } while (0)

#define RESTORE_CONTEXT(ctx)	\
    do {\
	SP = ctx.sp;\
	__asm__ __volatile__(\
"   pop	   r0\n"	/* load SREG from stack */\
"   out	   0x3f, r0\n"	/* restore SREG */\
"   pop    r31\n"	/* pop all the registers from the stack */\
"   pop    r30\n"\
"   pop    r29\n"\
"   pop    r28\n"\
"   pop    r27\n"\
"   pop    r26\n"\
"   pop    r25\n"\
"   pop    r24\n"\
"   pop    r23\n"\
"   pop    r22\n"\
"   pop    r21\n"\
"   pop    r20\n"\
"   pop    r19\n"\
"   pop    r18\n"\
"   pop    r17\n"\
"   pop    r16\n"\
"   pop    r15\n"\
"   pop    r14\n"\
"   pop    r13\n"\
"   pop    r12\n"\
"   pop    r11\n"\
"   pop    r10\n"\
"   pop    r9\n"\
"   pop    r8\n"\
"   pop    r7\n"\
"   pop    r6\n"\
"   pop    r5\n"\
"   pop    r4\n"\
"   pop    r3\n"\
"   pop    r2\n"\
"   pop    r1\n"\
"   pop    r0\n"\
	);\
    } while (0)

#endif /* End of CONTEXT_H */
