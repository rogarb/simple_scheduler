/******************************************************************************
* queue.h: Queue management API
 *****************************************************************************/
#ifndef QUEUE_H
#define QUEUE_H

#include "pthread.h"
#include "config.h"
#include "context.h"

#define QUEUE_CURRENT_NODE (t_queue.current)

#define QUEUE_CURRENT_NULL (QUEUE_CURRENT_NODE == NULL ? 1 : 0)

#define QUEUE_NEXT()	do {						    \
			    if (t_queue.current->next)			    \
				t_queue.current = t_queue.current->next;    \
			    else					    \
				t_queue.current = t_queue.head;		    \
			} while (0)

#ifndef WITH_HEAP
    #define CURRENT_CTX (t_queue.current->ctx)
#else
    #define CURRENT_CTX (t_queue.current->thread->ctx)

#define SAVE_CURRENT_CONTEXT()	    SAVE_CONTEXT(CURRENT_CTX)
#define RESTORE_CURRENT_CONTEXT()   RESTORE_CONTEXT(CURRENT_CTX)

struct queue_node {
    struct queue_node *next, *prev;
    struct pthread *thread;
};
#endif /* WITH_HEAP */

struct thread_queue {
#ifdef WITH_HEAP
    struct queue_node *head;
    struct queue_node *current;
    struct queue_node *tail;
#else
    struct pthread *head;
    struct pthread *current;
    struct pthread *tail;
#endif
};

extern struct thread_queue t_queue;

int thread_queue(struct pthread *t);
int thread_unqueue(struct pthread *t);

#endif /* End of QUEUE_H */
