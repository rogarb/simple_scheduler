/******************************************************************************
* timer.h: Timing related API
 *****************************************************************************/
#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>

extern volatile uint16_t ms_counter;

void timer_init(void);
void timer_on(void);
void timer_off(void);

#endif /* End of TIMER_H */
