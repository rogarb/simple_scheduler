/******************************************************************************
* errcodes.h: Error codes definition
 *****************************************************************************/
#ifndef ERRCODES_H
#define ERRCODES_H

enum errcodes { ERR = 1,  /* generic error */
	        ERR_NULL,  /* NULL pointer error */
		ERR_NOINIT, /* Queue is not initialized */
		ERR_ALREADYQUEUED, /* the thread is already in queue */
		ERR_NOMEM,  /* memory allocation error */
};

#endif /* End of ERRCODES_H */
