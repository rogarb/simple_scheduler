/******************************************************************************
* registers.h: registers related definitions
 *****************************************************************************/
#ifndef REGISTERS_H
#define REGISTERS_H

#include <avr/io.h>
#include <stdint.h>

#define REG_NUM 32  /* number of processor registers in the architecture */

/* We use the 8MHz builtin clock of the attiny85 so one instruction is 1µs */
#define CPU_FREQ 8000000

#define LOW8BITS(var16bits) ((uint8_t) ((uint16_t) var16bits & 0xFF))
#define HIGH8BITS(var16bits) ((uint8_t) ((uint16_t) var16bits >> 8))

#endif /* End of REGISTERS_H */
